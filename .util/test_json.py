import sys
import json

# Ensures a given json file is valid.

def load_json(filename):
    tmp = None
    with open(filename) as f:
        try:
            tmp = json.load(f)
            print("No errors detected.")
        except json.JSONDecodeError as err:
            print("There was an error detected in line", err.lineno,
                    "col", err.colno, "of file", sys.argv[1], ":")
            print(err.msg)
    return tmp

def check_valid_rooms(tmp):
    for index, room in enumerate(tmp):
        for key in ["name", "desc", "exits"]:
            if key not in room:
                fatal("Room[{}] missing key \"{}\"".format(index, key))
        exits = room.get("exits", None)
        if exits:
            rname = room.get("name", None)
            if rname and rname in exits:
                warning("Room[{}] has exit(s) leading to itself.".format(index))
            elif len(exits) == 0:
                warning("Room[{}] has no exits.".format(index))
    return

def check_valid_items(tmp):
    for index, item in enumerate(tmp):
        for key in ["name", "alias", "desc"]:
            if key not in item:
                fatal("Item[{}] missing key \"{}\"".format(index, key))
        alias = item.get("alias", None)
        if len(alias) == 0:
            warning("Item[{}] has no aliases.".format(index))
        funcname = item.get("func", None)
        if funcname:
            if not all([(c.isalnum() or c=="_") for c in funcname]):
                fatal("Item[{}] has an invalid function name.".format(index))
    return

def warning(s):
    print("--WARNING--:", s)
    return

def fatal(s):
    print("===FATAL===:", s)
    return

def main():
    # Assume we're in a subdirectory of the game file.
    gamedir = ".."
    print("Analysing room.json...")
    try:
        filename = gamedir + "/.muddata/room.json"
        tmp = load_json(filename)
    except FileNotFoundError:
        # Perhaps we're already in gamedir?
        gamedir = "."
        filename = gamedir + "/.muddata/room.json"
        tmp = load_json(filename)
    if tmp:
        check_valid_rooms(tmp)
    print("Analysing item.json...")
    filename = gamedir + "/.muddata/item.json"
    tmp = load_json(filename)
    if tmp:
        check_valid_items(tmp)
if __name__=="__main__":
    main()
