#!/usr/bin/python3

import socket

SOCKNUM = 31416

# Message codes -- must be the same in mud_server.py
SET_NAME    = "setname"
CODE_ACCEPT = "allgood"
CODE_DENY   = "nowaydude"
CODE_MULTI  = "moretocome"
CODE_READY  = "goahead"
CODE_QUIT   = "goodbye"
CODE_NORMAL = "normal"

def main():
    s = socket.socket()
    s.connect(("localhost", SOCKNUM))

    logged_in = False
    while not logged_in:
        # First message must be SET_NAME
        x = input("Name: ")
        msg = "{}:{}".format(SET_NAME, x)
        s.sendall(msg.encode("utf-8"))

        # Ensure connection was successful
        response = s.recv(1024).decode("utf-8")
        if response == CODE_ACCEPT:
            logged_in = True
        elif response == CODE_DENY:
            print("Try again.")

    send_input = True
    outputbuf = ""
    while True:
        if send_input:
            user_input = input("> ")
            msg = "{}:{}".format(CODE_NORMAL, user_input)
            s.sendall(msg.encode("utf-8"))
        response = s.recv(1024).decode("utf-8")
        if response.startswith(CODE_MULTI):
            # "moretocome:<output part n>"
            parts = response.split(":", 1)
            if parts[1]:
                outputbuf += parts[1]
            send_input = False
        elif response == CODE_READY:
            # "goahead"
            print(outputbuf)
            outputbuf = ""
            send_input = True
        elif response == CODE_QUIT:
            break

    s.close()
    return

if __name__=="__main__":
    main()
