#!/usr/bin/python3

import os
import socket
import selectors
import json
import random

SOCKNUM = 31416
EVENT_QUEUE = None
DATA_TEMP = {"socket":None, "addr":None, "output":[]}

GAME_DIR  = os.path.dirname(os.path.abspath(__file__))
DATA_DIR  = GAME_DIR + "/.muddata"
ROOM_DEF  = DATA_DIR + "/room.json"
ITEM_DEF  = DATA_DIR + "/item.json"
SAVE_FILE = DATA_DIR + "/save.json"

# Message codes -- must be the same in mud_client.py!
SET_NAME    = "setname"
CODE_ACCEPT = "allgood"
CODE_DENY   = "nowaydude"
CODE_MULTI  = "moretocome"
CODE_READY  = "goahead"
CODE_QUIT   = "goodbye"
CODE_NORMAL = "normal"

MAX_MSG_LENGTH = 800

ROOMS = []
ITEMS = []
CONNECTIONS = {}
# socket: player

LAST_SEEN = {
    "players": {},
#        {"MrFoo": "spawn room"},
    "items": {}
#        {"Scroll of missing code": "spawn room"}
}

### Structs ###

# player = {
#       "name": name,
#       ? "passhash": passhash,
#       "room": room_struct,
#       "outbufs": [<output buffer>, ...],
#       "items": [<item dict>, ...]
# }

# room = {
#       "name": name,
#       "desc": description,
#       "exits": {
#               key: roomname
#               ...
#               key: roomname
#       },
#       "players": [] # must be added manually
# }

# items = {
#       "name": name,
#       "desc": description,
#       "holder": player or None
# }

### Connection functions ###

def make_socket():
    stmp = socket.socket()
    stmp.bind(("localhost", SOCKNUM))
    SOCKET = stmp
    return stmp

def accept_wrapper(sock):
    global EVENT_QUEUE
    conn, addr = sock.accept()
    print("Accepted connection from", addr)
    conn.setblocking(False)
    data = DATA_TEMP.copy()
    data.update({"socket":conn, "addr":addr})
    events = selectors.EVENT_READ | selectors.EVENT_WRITE
    EVENT_QUEUE.register(conn, events, data=data)
    return conn

def process_request(key, mask):
    sock = key.fileobj
    data = key.data
    if mask & selectors.EVENT_READ:
        recv_data = sock.recv(1024).decode("utf-8")
        if recv_data:
            print("Received data from", data["addr"], ":")
            print(recv_data)
            player = CONNECTIONS.get(data["socket"], {})
            result = process_msg(player, recv_data)
            if result:
                data["output"].append(result)
            elif player["outbufs"]:
                data["output"] = player["outbufs"]
                player["outbufs"] = []
        else:
            # Client closed socket
            player = CONNECTIONS.get(data["socket"], {})
            remove_player(player)
            EVENT_QUEUE.unregister(sock)
            sock.close()
            print("Remote client", data["addr"], "closed connection.")
    if mask & selectors.EVENT_WRITE:
        while data["output"]:
            outbuf = data["output"].pop(0)
            if len(outbuf) > MAX_MSG_LENGTH:
                data["output"].insert(0, "{}:{}".format(
                                                CODE_MULTI,
                                                outbuf[MAX_MSG_LENGTH:]))
                outbuf = outbuf[:MAX_MSG_LENGTH]
            elif outbuf.startswith(CODE_MULTI) and CODE_READY not in data["output"]:
                data["output"].append(CODE_READY)
            print("Sending data to", data["addr"], ":")
            print(outbuf)
            sent = sock.send(outbuf.encode("utf-8"))
            # Ensure the rest of the message is eventually sent
            # data["output"].append(data["output"][sent:])
    return

def process_msg(player, input_string):
    """Processes a string sent from a client.
        code:args
    e.g.
        CODE_NORMAL:go north
    Returns CODE_ACCEPT, CODE_DENY, or None.
    If output is required, returns None and modifies player's outbufs."""
    if input_string:
        parts = input_string.split(":", 1)
        if parts[0] == SET_NAME and parts[1]:
            old_name = player["name"]
            new_name = "".join([c for c in parts[1] if c.isalpha()])
            if new_name and new_name not in [p["name"] for p in CONNECTIONS.values()]:
                print("Setting name to", new_name)
                player["name"] = new_name
                # If they're setting their name for the first time,
                # spawn them in.
                if not old_name:
                    add_player(player)
                return CODE_ACCEPT
            else:
                return CODE_DENY
        elif parts[0] == CODE_NORMAL:
            outbuf = ""
            if len(parts) > 1:
                args = parts[1].split()
            else:
                args = []
            do_command(player, args)
    return None

### Setup functions ###

def setup_rooms():
    rtmp = []
    with open(ROOM_DEF, "r") as f:
        rtmp = json.load(f)
    for r in rtmp:
        r["players"] = []
        r["items"] = []
    return rtmp

def setup_items():
    itmp = []
    with open(ITEM_DEF, "r") as f:
        itmp = json.load(f)
    for i in itmp:
        i["holder"] = None
    return itmp

def place_items():
    global ITEMS
    global ROOMS
    for i in ITEMS:
        if i["name"] in LAST_SEEN["items"]:
            r = get_room_by_name(LAST_SEEN["items"][i["name"]])
        else:
            r = ROOMS[0]
        r["items"].append(i)
    return

def save_state():
    """Record the location of any players and items."""
    global LAST_SEEN
    with open(SAVE_FILE, "w") as f:
        json.dump(LAST_SEEN, f)
    return

def load_state():
    """Restore the location of any players and items."""
    ltmp = {}
    with open(SAVE_FILE, "r") as f:
        ltmp = json.load(f)
    return ltmp

### Player modifying functions ###

def new_player():
    p = {
        "name": None,
        "room": None,
        "outbufs": [],
        "items": []
    }
    return p

def send_multi(player, message, *args):
    player["outbufs"].append("{}:{}".format(
        CODE_MULTI,
        message.format(*args)))
    return

def add_player(player):
    """Puts a new player into the last seen room."""
    global ROOMS
    global LAST_SEEN
    rname = LAST_SEEN["players"].get(player["name"], None)
    # get_room_by_name may return None.
    target_room = get_room_by_name(rname) if rname else None
    if not target_room:
        target_room = ROOMS[0]
    player_spawn(player, target_room)
    return

def remove_player(player):
    """Removes a player from the game after they quit."""
    # If player disconnects while setting their name, they might
    # not have been placed in a room yet.
    if player["room"]:
        player_despawn(player)
    for k, v in list(CONNECTIONS.items()):
        if v == player:
            del CONNECTIONS[k]
    return

### Action functions ###

def get_room_by_name(rname):
    global ROOMS
    for room in ROOMS:
        if room["name"].lower() == rname.lower():
            return room
    return None

def get_item_by_name(iname, allow_alias=True):
    global ITEMS
    for item in ITEMS:
        if item["name"].lower() == iname.lower():
            return item
        elif allow_alias and iname.lower() in [s.lower() for s in item["alias"]]:
            return item
    return None

def get_player_by_name(pname):
    global CONNECTIONS
    for player in CONNECTIONS.values():
        if player["name"] == pname:
            return player
    return None

def move_player(player, dst):
    global LAST_SEEN
    dst_room = None
    if dst in player["room"]["exits"]:
        dst_room = get_room_by_name(player["room"]["exits"][dst])
    if dst_room:
        # Leave src room
        if player in player["room"]["players"]:
            player["room"]["players"].remove(player)
        for p in player["room"]["players"]:
            send_multi(p, "{} left {}.\n",
                        player["name"], player["room"]["name"])
        # Enter dst room
        player["room"] = dst_room
        dst_room["players"].append(player)
        for p in dst_room["players"]:
            if p == player:
                send_multi(p, dst_room["name"].title())
            else:
                send_multi(p, "{} entered {}.\n",
                            player["name"], player["room"]["name"])

        LAST_SEEN["players"][player["name"]] = dst_room["name"]
        for i in player["items"]:
            LAST_SEEN["items"][i["name"]] = dst_room["name"]
        save_state()
        return
    else:
        print("Can't find room")
    return

def do_look(player):
    room = player["room"]
    send_multi(player, room["name"].title() + "\n")
    send_multi(player, room["desc"])
    if len(room["players"]) > 1:
        send_multi(player, "\n" + print_players(room["players"], player))
    if len(room["items"]) > 0:
        send_multi(player, "\n" + print_items(room["items"]))
    if len(room["exits"]) > 0:
        send_multi(player,
                    "\nExits:\n" + "\n".join(
                        ["  {}".format(e) for e in room["exits"].keys()]))
    return

def do_inventory(player):
    if len(player["items"]) == 0:
        send_multi(player, "You don't have anything.")
    else:
        send_multi(player, "You have:\n" + "\n".join(
                    ["  {}".format(i["name"]) for i in player["items"]]))

def do_say(player, sentence):
    for p in player["room"]["players"]:
        send_multi(p, "[{}]: {}\n", player["name"], sentence)
    return

def do_shout(player, sentence):
    for p in CONNECTIONS.values():
        send_multi(p, "[{}] (shouting): {}\n", player["name"], sentence)
    return

def do_examine(player, itemname):
    item = get_item_by_name(itemname)
    if item and (item in player["room"]["items"] or item in player["items"]):
        send_multi(player, item["desc"])
    else:
        send_multi(player, "You can't see that here.")
    return

def do_get_item(player, itemname):
    item = get_item_by_name(itemname)
    room = player["room"]
    output = ""
    if item in player["items"]:
        output = "You already have that!"
    elif item not in room["items"]:
        output = "You can't see that here."
    else:
        player_gets(player, item)
        output = "Taken."
    send_multi(player, output)
    return

def do_drop_item(player, itemname):
    item = get_item_by_name(itemname)
    output = ""
    if item not in player["items"]:
        output = "You don't have that!"
    else:
        player_drops(player, item)
        output = "Dropped."
    send_multi(player, output)
    return

def do_respawn(player):
    player_despawn(player)
    player_spawn(player, ROOMS[0])
    return

def do_use_item(player, item, args):
    if item not in player["items"]:
        send_multi(player, "You don't have that item.")
    elif "func" not in item:
        send_multi(player, "You don't know how to use that.")
    else:
        funcname = item["func"]
        if not all([(c.isalnum() or c=="_") for c in funcname]):
            print("Invalid funcname")
        else:
            func = eval("itemfunc_" + funcname)
            if func:
                func(player, item, args)
    return

def player_despawn(p):
    room = p["room"]
    for i in p["items"]:
        player_drops(p, i)
    if p in room["players"]:
        room["players"].remove(p)
    for other in room["players"]:
        send_multi(other, "{} dematerialises.", p["name"])
    return

def player_spawn(p, location):
    global LAST_SEEN
    for other in location["players"]:
        if other != p:
            send_multi(other, "{} materialises.", p["name"])
    if p["room"] and p in p["room"]["players"]:
        player_despawn(p)
    p["room"] = location
    location["players"].append(p)
    LAST_SEEN["players"][p["name"]] = location["name"]
    return

def player_gets(p, i):
    if i in p["room"]["items"]:
        p["room"]["items"].remove(i)
    if i not in p["items"]:
        p["items"].append(i)
    i["holder"] = p
    return

def player_drops(p, i):
    if i not in p["room"]["items"]:
        p["room"]["items"].append(i)
    if i in p["items"]:
        p["items"].remove(i)
    i["holder"] = None
    return

def print_players(plist, exclude):
    return "\n".join(
        ["{} is here.".format(p["name"]) for p in plist if p != exclude])

def print_items(ilist):
    return "\n".join(
        ["There is a{} {} here.".format(
                            "n" if i["name"][0].lower() in "aeiou" else "",
                            i["name"])
            for i in ilist])

def teleport(player, dst_room):
    global LAST_SEEN
    if player["room"] == dst_room:
        return
    # Leave src room
    if player in player["room"]["players"]:
        player["room"]["players"].remove(player)
    for p in player["room"]["players"]:
        send_multi(p, "{} vanishes in a flash!\n",
                    player["name"], player["room"]["name"])
    # Enter dst room
    player["room"] = dst_room
    dst_room["players"].append(player)
    for p in dst_room["players"]:
        if p == player:
            send_multi(p, dst_room["name"].title())
        else:
            send_multi(p, "{} appears in a flash!\n",
                        player["name"], player["room"]["name"])
    LAST_SEEN["players"][player["name"]] = dst_room["name"]
    for i in player["items"]:
        LAST_SEEN["items"][i["name"]] = dst_room["name"]
    save_state()
    return

### Item functions ###

# All item function names MUST:
# - have a name starting with "itemfunc_"
# - be prototyped as def itemfunc_f(player, args)
#   (where args is e.g. ["use", "sword", "on", "troll"])

def itemfunc_teleport_target(player, item, args):
    die_roll = random.randint(1, 20)
    if die_roll == 1:
        # Catastrophic failure
        target_room = random.choice(ROOMS)
        for p in player["room"]["players"]:
            send_multi(p, "{} {} malfunctions! Electrical arcs shoot out!",
                "Your" if p == player else "{}'s".format(player["name"]),
                item["name"])
            if target_room == player["room"]:
                send_multi(p, "But nothing else happens.")
        if target_room != player["room"]:
            player_despawn(player)
            player_spawn(player, target_room)
    else:
        # Assume target is the last element of args.
        target_name = args[-1]
        if target_name == "self" or target_name == "me":
            target = player
        else:
            target = get_player_by_name(target_name)
        if not target or target["room"] != player["room"]:
            send_multi(player, "You can't see that player here.\n")
        else:
            target_room = random.choice(ROOMS)
            for p in player["room"]["players"]:
                if p == player:
                    send_multi(p, "You zap your {} at {}!\n",
                        item["name"], target["name"])
                elif p == target:
                    send_multi(p, "{} zaps their {} at you!\n",
                        player["name"], item["name"])
                else:
                    send_multi(p, "{} zaps their {} at {}!\n",
                        player["name"], item["name"], target["name"])
                if target_room == target["room"]:
                    send_multi(p, "But nothing happens.\n")
            if target_room != target["room"]:
                teleport(target, target_room)
    return

### Main functions ###

def do_command(player, args):
    result = ""
    if args:
        verb = args[0]
    else:
        verb = ""
    if verb == "":
        player["outbufs"].append(CODE_READY)
    elif verb == "quit":
        player["outbufs"].append(CODE_QUIT)
    elif verb == "look" or verb == "l":
        do_look(player)
    elif verb == "inventory" or verb == "i":
        do_inventory(player)
    elif (verb == "go" and len(args) > 1) or verb in player["room"]["exits"]:
        if args[0] == "go":
            args.pop(0)
        dst = " ".join(args)
        move_player(player, dst)
    elif (verb == "say" and len(args) > 1):
        do_say(player, " ".join(args[1:]))
    elif (verb == "shout" and len(args) > 1):
        do_shout(player, " ".join(args[1:]))
    elif (verb == "examine" or verb == "x") and len(args) > 1:
        do_examine(player, " ".join(args[1:]))
    elif (verb == "get" or verb == "take") and len(args) > 1:
        do_get_item(player, " ".join(args[1:]))
    elif verb == "drop" and len(args) > 1:
        do_drop_item(player, " ".join(args[1:]))
    elif verb == "respawn":
        do_respawn(player)
    elif (verb == "use" or verb == "zap") and len(args) > 1:
        item = get_item_by_name(args[1])
        if item:
            do_use_item(player, item, args)
    else:
        send_multi(player, "Invalid command.")
    if not player["outbufs"]:
        player["outbufs"].append(CODE_READY)
    return

def main():
    sock = make_socket()
    global EVENT_QUEUE
    EVENT_QUEUE = selectors.DefaultSelector()

    global ROOMS
    ROOMS = setup_rooms()
    global LAST_SEEN
    # load state if available
    try:
        LAST_SEEN = load_state()
    except FileNotFoundError:
        pass

    # LAST_SEEN must be initialised before items are placed
    global ITEMS
    ITEMS = setup_items()
    place_items()

    global CONNECTIONS

    sock.listen()
    print("Listening on port", SOCKNUM, "...")
    sock.setblocking(False)
    EVENT_QUEUE.register(sock, selectors.EVENT_READ, data=None)

    while True:
        # Block until signal is received
        events = EVENT_QUEUE.select(timeout=None)
        for key, mask in events:
            if key.data == None:
                # New connection
                stmp = accept_wrapper(key.fileobj)
                p = new_player()
                CONNECTIONS[stmp] = p
                # Don't spawn them with add_player until they have a name
            else:
                # Existing connection
                # (key == the connected socket)
                process_request(key, mask)

if __name__=="__main__":
    main()
